package Programming;

import java.util.Arrays;

public class sortListOfStrings {
	
	public static void main(String args[]){
		
		String[] inputList = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"aug", "Sep", "Oct", "nov", "Dec" };
		
		//Display unsorted list
		System.out.println("-------Input List------");
		showlist(inputList);
		
		//Call to sort the list
		Arrays.sort(inputList);
		
		//Display the sorted list
		System.out.println("\n-------Sorted List-------");
		showlist(inputList);		
		
		// Call to sort the input list in case-sensitive order.
		System.out.println("\n-------Sorted list (Case-Sensitive)-------");
		Arrays.sort(inputList, String.CASE_INSENSITIVE_ORDER);
		
		// Display the sorted list.
		showlist(inputList);
		 
	}
	
	
	public static void showlist(String[] array){
		for(String str: array){
			System.out.print(str+" ");
		}
		
		System.out.println();
	}

}
