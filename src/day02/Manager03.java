package day02;

class Test03{
	void test(){
		System.out.println("I am from Test");
		new Manager03().displayAgain();
	}
}

public class Manager03 {
	public static void main(String[] args) {
		System.out.println("main starts");
		displayAgainAndAgain();
		new Manager03().display();
		System.out.println("main ends");
	}
	void display(){
		new Test03().test();;
		System.out.println("I am from display");
	}
	void displayAgain(){
		System.out.println("I am from displayAgain");
	}
	static void displayAgainAndAgain(){
		System.out.println("I am from displayAgainAndAgain");
	}
}
