package day02;

class Person{
	String name;
	int age;
	double weight;
}

public class Manager02 {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.name="Pooja";
		p1.age=20;
		p1.weight=20.34;
		
		Person p2= new Person();
		p2.name="Preeti";
		p2.age=202;
		p2.weight=340.98;
		
		p1=p2;
	}
}
