package day02;

class Bird{
	void drink(){
		System.out.println("drinks water");
	}
}

class Crow extends Bird{
	
}

class ChildCrow extends Crow{
	
}

public class Manager05_Inheritence {
	public static void main(String[] args) {
		Crow c= new Crow();
		c.drink();
		
		ChildCrow cc= new ChildCrow();
		cc.drink();
	}
}
