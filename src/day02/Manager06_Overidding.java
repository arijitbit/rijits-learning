package day02;

class Father06{
	void drive(){
		System.out.println("I drive Scooter");
	}
}
class Son06 extends Father06{
	@Override
	void drive() {
		System.out.println("I drive Bike");
	}
}

class Daughter extends Father06{
	@Override
	void drive() {
		System.out.println("I drive scooty");
	}
}

public class Manager06_Overidding {
	public static void main(String[] args) {
		new Son06().drive();
		new Daughter().drive();
		
		
	}
}
