package day02;
class A08{
	void A08(){
		System.out.println("DC-A08");
	}
}
class B08{
	B08(){
		System.out.println("DC-B08");
	}
	
	B08(int i){
		System.out.println("PC-B08");
	}
}
class C08 extends B08{
	C08(){
		System.out.println("DC-C08");
	}
	C08(int i, int j){
		System.out.println("PC-C08");
	}
}
public class Manager08_cons {
	public static void main(String[] args) {
		C08 c= new C08(12,34);
		
		//A08 a1= new A08();
		
		//B08 b= new B08();
		//B08 b1= new B08(10);
	}
}
