package day02;
class A09{
	void test(){
		System.out.println("Felling Sleepy");
	}
}
class B09 extends A09{
	@Override
	void test() {
		System.out.println("Feeling Hungry also");
	}
}
public class Manager09_final {
	public static void main(String[] args) {
		final int i=10;
		//i= i+10; final attributes cant be reinitialized
		System.out.println(i);
		
	}
}
