package day02;

class A07{
	A07(){
		System.out.println("DC-A07");
	}
	
	A07(int i){
		System.out.println("PC-A07");
	}
	A07(int i, int j){
		System.out.println("PC1-A07");
	}
	A07(int i, String s){
		System.out.println("PC2-A07");
	}
}

public class Manager07_cons {
	public static void main(String[] args) {
		A07 a= new A07();
		A07 a1= new A07(10);
		A07 a2= new A07(10,20);
		A07 a3= new A07(10,"Wip");
	}
}
