package day02;

class Test04{
	int i=10;
	void test(){
		int i=100;
		//System.out.println(new Test04().i);
		//System.out.println(this.i);
		this.testAgain();
	}
	void testAgain(){
		System.out.println(i);
	}
}

public class Manager04 {
	public static void main(String[] args) {
		Test04 t=new Test04();
		t.test();
		t.testAgain();
	}
	
	
}
